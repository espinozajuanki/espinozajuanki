### Hi there 👋

I´m a passionate software engineer specializing in ASP.NET and .NET Core technologies. Proficient in asp.Net core, HTML,CSS and JavaScript, experienced in version control with Git. 
Well-versed in Docker and Azure Cloud, and SQL databases like PostgreSQL and SQLServer. I am dedicated to staying ahead of the latest advancements in software development. Eager to contribute my skills to innovative projects and always excited about learning new technologies. Let's connect and explore opportunities in the dynamic world of software development!.
 
## <a href="https://www.linkedin.com/in/juan-carlos-espinoza-zumbado-16802a7b"><img src="https://img.shields.io/badge/-LinkedIn-2D2B55?style=flat-square&logo=linkedin&logoColor=white"/></a>


